//sydney sciascia
//to sort the arrays 
//cse 2
//professor carr
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

  public class CSE2Linear{ 
    
  public static void main(String[] args){
  Scanner myScanner = new Scanner(System.in);
    
      boolean correct =false; 
      int input=0; 
      int lastInt = 0;
      int counter = 0;
    
      System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
      //prompts the user to enter 15 ints for students final grades in CSE2
      int [] myList = new int[15]; 
      int [] grades  = new int [15];
    
        while(correct || counter <=14 ){
      correct = myScanner.hasNextInt();
  
       if (correct){
        input=myScanner.nextInt();
      
         }
          else {
      System.out.println("Error, enter again.");
      myScanner.next();
          }
         
          
     //Check that the user only enters ints, and print an error message if the user enters anything other than an int.
    while (input<0 || input >100){
      System.out.println("Error: out of range, enter again.");
      myScanner.next();
      
      
    //error message for an int that is out of the range from 0-100,
    if (counter == 0){
      grades[counter++] = input;
    }
          else{
       for (int i=0; i<= counter ; i++){
      if( grades [counter-1] > input){
        correct = false;
        System.out.println("Error: the int is not greater than or equal to the last int, enter again");
      break;
      }
       }
    }
    }
          if (correct){
            grades[counter++] = input;
          }
          
        }

      for (int i=0; i < myList.length ; i++){
	      System.out.print(myList[i] + " ");
        //Print the final input array.
     }
      //prompt the user to enter a grade to be searched for
    
      System.out.println("Scrambled array: " +Arrays.toString(grades));
    
      System.out.println("Enter a grade to search for: ");
      int key = myScanner.nextInt();
      int placeWhere = binarySearch(grades, key);
      int [] scrambled= new int [15];
       scrambled = randomScramble(grades);
    System.out.println("Scrambled array: " +Arrays.toString(scrambled));
      System.out.println("Enter a grade to search for: ");

       key = myScanner.nextInt();
      linearSearch(grades, key);
  }
    
    
      
      public static int binarySearch(int[] grades, int key) {
      
      // Use binary search to find the entered key
      //wrtitten in class w professor carr
      int low = 0;
      int high = grades.length-1;
      int counter =0; 
      
      
	   while(high >= low) {
	     int mid = (low + high)/2;
       if (key < grades[mid]) {
			  	high = mid - 1;
			  }
		   	else if (key == grades[mid]) {
			  	return mid;
          //System.out.println("The grade was found");
			  }
			  else {
			  	low = mid + 1;
			  }
       
       counter ++;
		}
        System.out.println("The amount of iterations through the binary search was :" + counter);
		 return -1;
      
  }
    
    
    //scramble it
 public static int[] randomScramble(int[] grades) {
   Random rand = new Random(); 
   int var = rand.nextInt(15)+1;
   
    for (int i =0; i <grades.length ; i++){ 
      grades[var]=i;
      int hold = grades[i];
      grades[i]=grades[var];
      grades[var]= hold;
    }
   for (int i=0; i < grades.length ; i++){
     return grades;
	//System.out.print("Scrambled" + grades[i] + " "); //to print 
  
    }
   return grades;
 }
    
  public static int linearSearch(int[] grades, int key){
	/** The method for finding a key in the list */
    //wrtitten in class w professor carr
    int counter=0;
    
		for (int i = 0; i < grades.length; i++) {
			if (key == grades[i]) 
        return i;
     // System.out.println("The grade was found");
      counter++;
		}
System.out.println("The amount of iterations through the binary search was :" + counter);
	return -1;
   
  }
    
	}
  
 
    
    