//sydney sciascia 
//cse2 
//october 23
//input is the number of rows and columns
//output is the rows and columns with stars and spaces 

import java.util.Scanner;

public class EncryptedX{
  
  public static void main(String args[]){
    
     Scanner myScanner = new Scanner(System.in);
    
     boolean correct =false; 
      int input=0; 
    
    System.out.println("Input an integer between 0 - 100 for the number of stars");
    
    //checks to see if user inputted an int 
      while(!correct){
      correct = myScanner.hasNextInt();
  
       if (correct){
      input=myScanner.nextInt();
    }
    else {
      System.out.println("Error, enter again.");
      myScanner.next();
    }
     }
    
    //checks to see if int is between one and 100 
    while (input <0 || input >100){
       System.out.println("Error, enter again.");
       input=myScanner.nextInt();
    }
    
    //for loop that goes through the rows 
     for (int i = 0 ; i <= input ; i ++){
       //for loop goes through the columns 
       for (int j=0; j<=input; j++){
      
         //if statement checks to see if j is = i for the line going through the right side
         //or statment checks to see if the line goes through the left 
         if (j==i || (j==(input-i))){
            System.out.print(" ");
         }
         
         else 
          System.out.print("*");
       }
       //prints new line 
       System.out.println("");
    }
      
    
    
  }
}