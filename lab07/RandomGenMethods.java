
import java.util.Random;
import java.util.Scanner;

public class RandomGenMethods{
  
  public static void main(String args[]){
     Scanner myScanner = new Scanner(System.in);
     Random randomGenerator = new Random();

    String adj = adjectives();
    String nSub = nounSub();
    String ver = verbs();
    String nObj = nounObj();
    
    System.out.println(adj+" "+nSub+" "+ver+" "+nObj);
    
    System.out.println("Do you want to print another sentence?");
    
    String more = myScanner.nextLine();
    
    while (more.equals("yes")){
      
          System.out.println(adj+" "+nSub+" "+ver+" "+nObj);
          more = myScanner.nextLine();
    }
    
    
     }
  
  public static String adjectives(){

    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    //switch statement
 
        switch (randomInt){
          case 1:
             return "aggressive";
             
          case 2:
            return "brave";

          case 3:
             return "calm";
          
          case 4:
            return "eager";
          
          case 5:
             return "happy";
          
            case 6:
              return "nice";
           
            case 7:
              return "smart";
            
            case 8:
              return "silly";
            
            case 9:
              return "rude";
            
            case 0:
              return "cheerful";
             default:
       return " ";
             
        }
   
  }
  
  public static String nounSub (){
      Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
 switch (randomInt){
          case 1:
             return "Frank";
           
          case 2:
            return "Sally";
         
          case 3:
             return "Jeff";
           
          case 4:
            return "Julia";
         
          case 5:
             return "Cat";
        
            case 6:
              return "chicken";
            
            case 7:
              return "Hellen";
           
            case 8:
              return "Jill";
       
            case 9:
              return "Cynthia";
           
            case 0:
              return "Hillary";
          default:
       return " ";
 }
    
  }
  
  public static String verbs (){
          Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);

      switch (randomInt){
          case 1:
             return "accepted";
           
          case 2:
            return "added";
      
          case 3:
             return "jumped";
           
          case 4:
            return "ran";
         
          case 5:
             return "swam";
         
            case 6:
              return "skipped";
            
            case 7:
              return "jogged";
        
            case 8:
              return "amused";
           
            case 9:
              return "asked";
           
            case 0:
              return "said";
            default:
       return " ";
      }
   
  }
  
  public static String nounObj (){
          Random randomGenerator = new Random();
   int randomInt = randomGenerator.nextInt(10);
 switch (randomInt){
          case 1:
             return "pig";
            
          case 2:
            return "bird";
          
          case 3:
             return "train";
           
          case 4:
            return "horse";
        
          case 5:
             return "seahorse";
          
            case 6:
              return "math";
            
            case 7:
              return "school";
           
            case 8:
              return "frog";
           
            case 9:
              return "zoo";
           
            case 0:
              return "airport"; 
           default:
           return " ";
  }

    
 }
     
    
  }

