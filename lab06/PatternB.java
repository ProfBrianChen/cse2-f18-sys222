//sydney sciascia
//cse 2
//pattern b 

import java.util.Scanner;

public class PatternB{
  
  public static void main(String args[]){
    
     Scanner myScanner = new Scanner(System.in);
    
 //   ask for number between 1 and 10
    
    boolean correct = false; 
    int input = 0 ;
   System.out.println("Provide a number between 1 and 10:");
  
      
        while(!correct){
      correct = myScanner.hasNextInt();
  
       if (correct){
      input=myScanner.nextInt();
    }
    else {
      System.out.println("Error, enter again.");
      myScanner.next();
    }
     }
    while (input > 10 || input < 1){
       System.out.println("Error, enter again.");
       input=myScanner.nextInt();
    }
    
    for (int i = input; i >= 1; --i){
      
      for (int j=1; j <= i; ++j){
        System.out.print(j +" ");
       j= j--;
      }
      System.out.println();
    }
    
    
  }
}

