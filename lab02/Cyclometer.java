   //sydney sciascia
  //sep 6 2018
  //cse 002

   //program to measure speed and distance

     public class Cyclometer {
         // main method required for every Java program
        public static void main(String[] args) {

          //declare the variables of seconds
        int secsTrip1=480;  //to count the time of the first trip
        int secsTrip2=3220;  // to count the time of second trip
        int countsTrip1=1561;  //to count the time of the 3rd trip
        int countsTrip2=9037; //to count the time of the 4th trip
      //declare the variables of seconds


      // other constants  
      double wheelDiameter=27.0,  //diameter
      PI=3.14159, // pie
      feetPerMile=5280,  //how many feet in a mile
      inchesPerFoot=12,   //how many inches in a foot
      secondsPerMinute=60;  //how many seconds in a min
      double distanceTrip1, distanceTrip2,totalDistance;  //declares the doubles for the 2 trips and the distance 
     // other constants  

//print the numbers stored in the variables and convert into minutes and counts
  System.out.println("Trip 1 took "+  (secsTrip1/secondsPerMinute)+" minutes and had "+  countsTrip1+" counts.");
  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
//print the numbers stored in the variables and convert into minutes and counts
          
          
     //calculating the distance of the trip
          
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
          
          //Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
   //Print out the output data.



          
        }  //end of main method   
      } //end of class






