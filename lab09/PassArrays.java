import java.util.Scanner;
  public class PassArrays{ 
    
  public static void main(String[] args){
  Scanner scan = new Scanner(System.in);
    int [] array0 = {2, 3, 4, 5, 6 ,7, 8, 9};
    int [] array1 = copy(array0);
    int [] array2 = copy(array0);
    
    inverter(array0);
    print(array0);
    System.out.println("");
    array1 = inverter2(array0);
    print(array1);
    inverter2(array2);
    System.out.println("");
    
    int [] array3 = new int[array2.length];
    for (int i =0; i < array2.length ; i++){
      array3[i]=array2[i];
    }
    print(array3);
  }
    public static int [] copy(int[] list){
      int [] myList = new int[list.length];
        for (int i=0; i <list.length; i ++){ //fill in one array with another array 
          myList[i]=list[i];
        }
      return myList;
    }
    
    public static void inverter(int[] list){
      int counter = list.length-1;
      int temp = 0;
      
      for (int i= 0; i<list.length/2; i++){
        temp= list[i];
        list[i]=list[counter-i]; // swap the first element with the last element 
        list[counter-i] = temp;
        
      }
    }
    
    public static int [] inverter2(int[] list){
      copy(list);
      inverter(list);
      return list;
    }
    
    public static void print(int[] list){
      for (int i=0; i < list.length ; i++){
	      System.out.print(list[i] + " ");
    }
  
  }
  }