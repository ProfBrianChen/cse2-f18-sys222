import java.util.Scanner;

public class Convert{
  
  public static void main(String args[]){
   
    Scanner myScanner = new Scanner(System.in);
    
    
    
    System.out.print("Enter the affected area in acres: ");
    double affectedArea = myScanner.nextDouble();
    //creates a scanner to input the number of area affected by rain
    
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall = myScanner.nextDouble();
    //creates a scanner to find the rainfail in the area
    
    double acreInches = affectedArea * rainfall;
    //converts affectedarea and rainfall into acre inches
       
    double gallons = acreInches * (27154.285990761);
   // cubic inches to gallons 
     
    double cubicMiles = gallons * .000000000000908169;
    //converts gallons to cubic miles
    
    
    System.out.println(cubicMiles+ " cubic miles");
    //prints the output
    
  }


}
    