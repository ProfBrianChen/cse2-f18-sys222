public class Arithmetic{
  
  public static void main(String args[]){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;

    
    double totalCostOfPants;   //total cost of pants no sales tax
    double totalCostOfShirts;  //total cost of shirts no sales tax
    double totalCostOfBelts;   //total cost of belts no sales tax
    
      
    
    //calculating the total cost of 3 pairs of pants w/o sales tax
    totalCostOfPants= pantsPrice*numPants;
    System.out.println("The total cost of 3 pairs of pants without sales tax is: $"+totalCostOfPants);
    
    //calculating the total cost of 2 shirts w/o sales tax
    totalCostOfShirts= numShirts*shirtPrice;
    System.out.println("The total cost of 2 shirts without sales tax is: $"+totalCostOfShirts);
    
    //calculating the total cost of 1 belt w/o sales tax
    totalCostOfBelts= numBelts*beltCost;
    System.out.println("The total cost of 1 belt without sales tax is: $"+totalCostOfBelts +"\n");
    
    
    //calculating the sales tax on pants
    double salesTaxPants=totalCostOfPants*paSalesTax;
    //making sales tax into a 2 digit decimal
    salesTaxPants=(100)*salesTaxPants;
    salesTaxPants=(int)salesTaxPants;
    salesTaxPants=(salesTaxPants)/(100.0);
    double salesTPants = salesTaxPants;
    
    
    System.out.println("The sales tax on pants is: $"+salesTaxPants);
    //calculating the sales tax on pants
    
    
    //calculating the sales tax on shirts
    double salesTaxShirts=totalCostOfShirts*paSalesTax;
    //making sales tax into a 2 digit decimal
    salesTaxShirts=(100)*salesTaxShirts;
    salesTaxShirts=(int)salesTaxShirts;
    salesTaxShirts=(salesTaxShirts)/(100.0);
    double salesTShirts = salesTaxShirts;
    
    System.out.println("The sales tax on shirts is: $"+salesTaxShirts);  
    //calculating the sales tax on shirts
    
    
    
    //calculating the sales tax on belts
    double salesTaxBelts=totalCostOfBelts*paSalesTax;
    //making sales tax into a 2 digit decimal
    salesTaxBelts=(100)*salesTaxBelts;
    salesTaxBelts=(int)salesTaxBelts;
    salesTaxBelts=(salesTaxBelts)/(100.0);
    double salesTBelts = salesTaxBelts;
    
    System.out.println("The sales tax on belts is: $"+salesTaxBelts+"\n");
    //calculating the sales tax on belts
    
    
    //total cost of the purchase 
    double totalPurchaseNoTax= totalCostOfBelts+totalCostOfPants+totalCostOfShirts;
    System.out.println("The total cost of the purchase without tax is: $" +totalPurchaseNoTax);
   //total cost of the purchase 
    
    //total sales tax
    double totalSalesTax = salesTShirts+salesTBelts+salesTPants;
    totalSalesTax=(100)*totalSalesTax;
    totalSalesTax=(int)totalSalesTax;
    totalSalesTax=(totalSalesTax)/(100.0);
    
    System.out.println("The total cost of tax is: $" +totalSalesTax);
    //total sales tax
    
    //total cost of it all
    double totalOfEverything=totalSalesTax+totalPurchaseNoTax;
    System.out.println("The total cost of the transaction, including tax, is: $"+totalOfEverything);
    
    
  
  }

  
}


