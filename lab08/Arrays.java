
import java.util.Random;
import java.util.Scanner;
import java.util.*;

public class Arrays{
  
    public static void main(String args[]){
      
     Scanner myScanner = new Scanner(System.in);
    
      
      //creates two new arrays with 100 spaces
     int [] myList = new int[100];
     int [] myListTwo = new int[100];
      
      //puts random ints in each index of array1
     for (int i =0; i < myList.length ; i++){
       myList[i]=(int)(Math.random()*99)+0;
    
      }
      
      //prints array 1
     System.out.print("Array 1 holds the following integers: ");
     for (int i=0; i < myList.length ; i++){
           System.out.print(myList[i] + " "); //to print 
      }
      
      
      //increments index if the value in the array occurs more than once
      for (int i = 0; i<myList.length; i++){
        for (int j = 0; j <=99; j ++){
         if (myList[i] == j){
           myListTwo[j] +=1;
          }
        }
      }
      
      //prints the second array if the value isnt equal to 0
      for(int i=0; i <=99;i ++){
        if (myListTwo[i] !=0 ){
        System.out.println(i+ " occurs " +myListTwo[i]);
        }
        else {
          System.out.print("");
        }
      }
      

  
  }
}
